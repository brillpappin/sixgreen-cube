package sixgreen.cube.util;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ParamUtilTest {
    @Test
    public void emptyArgsArrayConvertedToNull() throws Exception {
        String[] actual = ParamUtil.fixArgs(new String[0]);
        assertNull("The args array should be null.", actual);
    }

    @Test
    public void notEmptyArgsArrayPassesThrough() throws Exception {
        String[] actual = ParamUtil.fixArgs(new String[]{"a"});
        assertNotNull("The args array should not be null.", actual);
        assertEquals(1, actual.length);
        assertEquals("a", actual[0]);
    }

    @Test
    public void nullArgsArrayPassesThrough() throws Exception {
        String[] actual = ParamUtil.fixArgs(null);
        assertNull("The args array should be null.", actual);
    }
}

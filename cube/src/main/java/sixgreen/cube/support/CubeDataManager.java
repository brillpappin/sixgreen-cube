package sixgreen.cube.support;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import sixgreen.cube.Cube;
import sixgreen.cube.CubeConfig;
import sixgreen.cube.annotation.Id;
import sixgreen.cube.annotation.Table;
import sixgreen.cube.annotation.View;
import sixgreen.cube.util.NameUtil;
import sixgreen.cube.util.ParamUtil;
import sixgreen.cube.util.ReflectionUtil;

/**
 * Created by bpappin on 16-08-23.
 */
public class CubeDataManager extends SQLiteOpenHelper {
    private static final String TAG = "CubeDataManager";

    private final CubeConfig config;
    private final Map<Object, Long> entitiesMap = new HashMap<Object, Long>();
    private SQLiteDatabase database;
    private Context context;

    ///**
    // * Create a helper object to create, open, and/or manage a database.
    // * This method always returns very quickly.  The database is not actually
    // * created or opened until one of {@link #getWritableDatabase} or
    // * {@link #getReadableDatabase} is called.
    // *
    // * @param context
    // *         to use to open or create the database
    // * @param name
    // *         of the database file, or null for an in-memory database
    // * @param factory
    // *         to use for creating cursor objects, or null for the default
    // * @param version
    // *         number of the database (starting at 1); if the database is older,
    // *         {@link #onUpgrade} will be used to upgrade the database; if the database is
    // *         newer, {@link #onDowngrade} will be used to downgrade the database
    // */
    //public CubeDataManager(Context context, String name, CursorFactory factory, int version) {
    //    super(context, name, factory, version);
    //}

    /**
     * Create a helper object to create, open, and/or manage a database.
     * The database is not actually created or opened until one of
     * {@link #getWritableDatabase} or {@link #getReadableDatabase} is called.
     * <p/>
     * <p>Accepts input param: a concrete instance of {@link DatabaseErrorHandler} to be
     * used to handle corruption when sqlite reports database corruption.</p>
     *
     * @param context to use to open or create the database
     * @param config  the configuration of the Cube framework that contains the information for
     *                opening the database and setting up its parameters.
     */
    public CubeDataManager(Context context, CubeConfig config) {
        super(context, config.getDatabaseName(), config.getCursorFactory(), config
                .getVersion(), config.getDatabaseErrorHandler());
        this.config = config;
        this.context = context;
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        try {
            db.execSQL("PRAGMA auto_vacuum = FULL");
            Log.w(Cube.TAG, "On Configure: Auto VACUUM enabled.");
        } catch (Exception e) {
            Log.w(Cube.TAG, "On Configure: Setting Auto VACUUM failed.", e);
        }
        super.onConfigure(db);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        if (getConfig().isDebug()) {
            Log.d(Cube.TAG, "On Create: " + db.getVersion());
        }
        SchemaGenerator schemaGenerator = SchemaGenerator.get(getConfig());
        schemaGenerator.doCreate(db);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (getConfig().isDebug()) {
            Log.d(Cube.TAG, "On Upgrade: " + oldVersion + " -> " + newVersion);
        }
        SchemaGenerator schemaGenerator = SchemaGenerator.get(getConfig());
        schemaGenerator.doUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Called when the database needs to be downgraded. This is strictly similar to
     * {@link #onUpgrade} method, but is called whenever current version is newer than requested
     * one.
     * However, this method is not abstract, so it is not mandatory for a customer to
     * implement it. If not overridden, default implementation will reject downgrade and
     * throws SQLiteException
     * <p/>
     * <p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (getConfig().isDebug()) {
            Log.d(Cube.TAG, "On Downgrade: " + oldVersion + " -> " + newVersion);
        }
        //super.onDowngrade(db, oldVersion, newVersion);
        SchemaGenerator schemaGenerator = SchemaGenerator.get(getConfig());
        schemaGenerator.doDowngrade(db, oldVersion, newVersion);
    }

    public void onReset() {
        if (getConfig().isDebug()) {
            Log.d(Cube.TAG, "On Reset");
        }
        SchemaGenerator schemaGenerator = SchemaGenerator.get(getConfig());
        schemaGenerator.doManualReset(getDatabase());
    }

    //public void close() {
    //    if (database != null) {
    //        database.close();
    //    }
    //}

    public SQLiteDatabase getDatabase() {
        if (database == null) {
            if (getConfig().isDebug()) {
                Log.d(Cube.TAG, "Getting writable database.");
            }
            database = getWritableDatabase();
        }

        return database;
    }

    public Map<Object, Long> getEntitiesMap() {
        return entitiesMap;
    }

    public CubeConfig getConfig() {
        return config;
    }

    public Context getContext() {
        return context;
    }

    public <T> Cursor getCursor(Class<T> type, String whereClause, String[] whereArgs, String groupBy, String orderBy, int limit) {
        if (getConfig().isDebug()) {
            Log.d(Cube.TAG,
                  "Getting cursor from database for type: " + type.getSimpleName() + " where " +
                          whereClause);
        }
        Cursor raw = getDatabase()
                .query(NameUtil.toTableName(type), null, whereClause, ParamUtil.fixArgs(whereArgs),
                       groupBy, null, orderBy, Integer.toString(limit));
        return raw;
    }

    private void inflate(Cursor cursor, Object entity, Map<Object, Long> entitiesMap) {
        if (getConfig().isDebug()) {
            Log.d(Cube.TAG,
                  "Inflating entity from cursor for type: " + entity
                          .getClass()
                          .getSimpleName());
        }
        //if (DEBUG_CURSOR) {
        //    Log.d(TAG, "Row Dump (inflate): " + DatabaseUtils.dumpCurrentRowToString(cursor));
        //}


        List<Field> columns = ReflectionUtil.getTableFields(getConfig(), entity.getClass());

        if (entity != null && !entity
                .getClass()
                .isAnnotationPresent(View.class)) {
            // FIXME View's don't have ID, so we cant keep a ref to them.
            // FIXME in the future we will want to cache static views and not temporary views.
            if (!entitiesMap.containsKey(entity)) {
                entitiesMap.put(entity, cursor.getLong(cursor.getColumnIndex(Cube._ID)));
            }
        }

        for (Field field : columns) {
            field.setAccessible(true);
            Class<?> fieldType = field.getType();
            if (Cube.isEntity(fieldType)) {
                // This is a joined entity.
                try {
                    // XXX this is the ID of the joined record, not the parent entity ID.
                    long id = cursor.getLong(cursor
                                                     .getColumnIndex(NameUtil.toColumnName(field)));
                    field.set(entity, (id > 0) ? load(fieldType, id) : null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                ReflectionUtil.setFieldValueFromCursor(getConfig(), cursor, field, entity);
            }
        }
    }

    public <T> List<T> getEntitiesFromCursor(Cursor cursor, Class<T> type) {
        T entity;
        List<T> result = new ArrayList<T>();
        try {
            while (cursor.moveToNext()) {
                entity = type
                        .getDeclaredConstructor()
                        .newInstance();
                inflate(cursor, entity, getEntitiesMap());
                result.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return result;
    }

    public void executeQuery(String query, String... arguments) {
        if (getConfig().isLogQueries()) {
            Log.d(Cube.TAG, "executeQuery: " + query);
        }
        getDatabase().execSQL(query, ParamUtil.fixArgs(arguments));
    }

    public Cursor rawQuery(String query, String... arguments) {
        if (getConfig().isLogQueries()) {
            Log.d(Cube.TAG, "rawQuery: " + query);
        }
        return getDatabase().rawQuery(query, ParamUtil.fixArgs(arguments));
    }

    public <T> T first(Class<T> type) {
        if (type.isAnnotationPresent(View.class)) {
            // XXX a view is not a record.
            Log.w(TAG, "Can't find a first record from a @View.");
            return null;
        }
        List<T> list = findWithQuery(type,
                                     "SELECT * FROM " +
                                             NameUtil.toTableName(type) + " ORDER BY " +
                                             Cube._ID +
                                             " ASC LIMIT 1");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public <T> T last(Class<T> type) {
        if (type.isAnnotationPresent(View.class)) {
            // XXX a view is not a record.
            Log.w(TAG, "Can't find a last record from a @View.");
            return null;
        }
        List<T> list = findWithQuery(type,
                                     "SELECT * FROM " +
                                             NameUtil.toTableName(type) + " ORDER BY " +
                                             Cube._ID + " DESC LIMIT 1");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public <T> List<T> view(Class<T> type) {
        if (!type.isAnnotationPresent(View.class)) {
            return null;
        }
        return find(type, null, null);
    }

    public <T> List<T> view(Class<T> type, String whereClause, String[] whereArgs) {
        if (!type.isAnnotationPresent(View.class)) {
            return null;
        }

        return find(type, whereClause, ParamUtil.fixArgs(whereArgs));
    }

    public <T> T load(Class<T> type, long id) {
        // FIXME Views don't have IDs
        // FIXME seems YAGNI to create a list, refactor.
        List<T> list = find(type, QueryBuilder
                .id(), new String[]{String.valueOf(id)}, null, null, 1);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public <T> List<T> load(Class<T> type, String[] ids) {
        // FIXME Views don't have IDs
        String whereClause =
                Cube._ID + " IN (" + QueryBuilder.generatePlaceholders(ids.length) + ")";
        return find(type, whereClause, ids);
    }

    public <T> List<T> find(Class<T> type, String whereClause, String... whereArgs) {
        // FIXME Views don't have IDs
        return find(type, whereClause, ParamUtil.fixArgs(whereArgs), null, null, 0);
    }

    public <T> List<T> find(Class<T> type, String whereClause, String[] whereArgs, String groupBy, String orderBy, int limit) {
        Cursor cursor = null;
        if (limit > 0) {
            cursor = getDatabase()
                    .query(NameUtil.toTableName(type), null, whereClause, ParamUtil.fixArgs(whereArgs),
                           groupBy, null, orderBy, Integer.toString(limit));
            return getEntitiesFromCursor(cursor, type);
        } else {
            return find(type, whereClause, ParamUtil.fixArgs(whereArgs), groupBy, orderBy);
        }
    }

    public <T> List<T> find(Class<T> type, String whereClause, String[] whereArgs, String groupBy, String orderBy) {
        Cursor cursor = getDatabase()
                .query(NameUtil.toTableName(type), null, whereClause, ParamUtil.fixArgs(whereArgs),
                       groupBy, null, orderBy);

        return getEntitiesFromCursor(cursor, type);
    }

    //public <T> T load(Class<T> type, Integer id) {
    //    return findById(type, Long.valueOf(id));
    //}

    /**
     * Save an entity to the database.
     *
     * @param entity
     * @param <T>
     * @return
     */
    public <T> long save(T entity) {
        //return save(getDatabase(), entity);
        return saveLegacy(getDatabase(), entity);
    }

    /**
     * Save a collection of entities to the database.
     *
     * @param entities
     * @return
     */
    public <T> void save(Collection<T> entities) {
        for (T entity : entities) {
            save(entity);
        }
    }

    // FIXME this is broken.
    long save(SQLiteDatabase db, Object object) {
        if (object
                .getClass()
                .isAnnotationPresent(View.class)) {
            // XXX can't save a view.
            return 0;
        }

        Map<Object, Long> entitiesMap = getEntitiesMap();

        List<Field> fields = ReflectionUtil.getTableFields(config, object.getClass());

        ContentValues values = new ContentValues(fields.size());

        Field idField = null;
        for (Field field : fields) {
            // XXX This used to allow the overwriting of a record wth the same ID.
            // XXX Revisit this as that may be a useful construct, however the way it
            // XXX works needs to be fixed so that it can handle null and zero.
            // don't include the _id in our list of insert/update values. the id never changes.
            if (field.isAnnotationPresent(Id.class)) {
                // used to be: field.getName().equals("id")
                idField = field;
            } else {
                ReflectionUtil.addFieldValueToColumn(config, values, field, object, entitiesMap);
            }
        }

        // XXX Why are we setting the _id again?
        //boolean isEntity = ReflectionUtil.isEntity(object.getClass());
        //if (isEntity && entitiesMap.containsKey(object)) {
        //    values.put(Cube._ID, entitiesMap.get(object));
        //}

        // XXX Replace likley isnt want we want here.
        long id = db.insertWithOnConflict(NameUtil.toTableName(object.getClass()), null, values,
                                          SQLiteDatabase.CONFLICT_REPLACE);

        if (object
                .getClass()
                .isAnnotationPresent(Table.class)) {
            if (idField != null) {
                idField.setAccessible(true);
                try {
                    idField.set(object, id);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                entitiesMap.put(object, id);
            }
        }
        //else if (SugarRecord.class.isAssignableFrom(object.getClass())) {
        //    ((SugarRecord) object).setId(id);
        //}


        if (getConfig().isDebug()) {
            Log.i(Cube.TAG, object
                    .getClass()
                    .getSimpleName() + " saved : " + id);
        }

        notifyChange(object.getClass(), id);


        return id;
    }

    @Deprecated
    long saveLegacy(SQLiteDatabase db, Object object) {
        if (object
                .getClass()
                .isAnnotationPresent(View.class)) {
            // XXX can't save a view.
            return 0;
        }
        Map<Object, Long> entitiesMap = getEntitiesMap();
        List<Field> fields = ReflectionUtil.getTableFields(getConfig(), object.getClass());
        ContentValues values = new ContentValues(fields.size());
        Field idField = null;
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                // during save, the id field is left alone and auto-generated by the database.
                idField = field;
            } else {
                ReflectionUtil
                        .addFieldValueToColumn(getConfig(), values, field, object, entitiesMap);
            }
        }

        boolean isEntity = Cube.isEntity(object.getClass());
        //if (isEntity && entitiesMap.containsKey(object)) {
        // XXX WE dont set the ID on save.
        //values.put("id", entitiesMap.get(object));
        //values.put(Cube._ID, entitiesMap.get(object));
        //}

        long id = db.insertWithOnConflict(NameUtil.toTableName(object.getClass()), null, values,
                                          SQLiteDatabase.CONFLICT_REPLACE);

        if (object
                .getClass()
                .isAnnotationPresent(Table.class)) {
            if (idField != null) {
                idField.setAccessible(true);
                try {
                    idField.set(object, new Long(id));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            // Add it to the entity map.
            entitiesMap.put(object, id);

        }
        //else if (SugarRecord.class.isAssignableFrom(object.getClass())) {
        //    ((SugarRecord) object).setId(id);
        //}

        notifyChange(object.getClass(), id);

        return id;
    }


    /**
     * Update does not automatically create a new object of the update object does not match.
     *
     * @param object
     * @return long the number of records changed by this update.
     */
    public long update(Object object) {
        return update(getDatabase(), object);
    }

    public <T> boolean update(Collection<T> entities) {
        int rowschanged = 0;
        for (T entity : entities) {
            rowschanged += update(entity);
        }
        return rowschanged > 0;
    }

    long update(SQLiteDatabase db, Object object) {
        if (object
                .getClass()
                .isAnnotationPresent(View.class)) {
            // XXX can't update a view.
            return 0;
        }
        Map<Object, Long> entitiesMap = getEntitiesMap();
        List<Field> fields = ReflectionUtil.getTableFields(getConfig(), object.getClass());
        ContentValues values = new ContentValues(fields.size());

        StringBuilder whereClause = new StringBuilder();
        List<String> whereArgs = new ArrayList<>();

        Field idField = null;
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                idField = field;
                try {
                    idField.setAccessible(true);
                    String columnName = NameUtil.toColumnName(idField);
                    Object idColumnValue = idField.get(object);

                    whereClause.append(QueryBuilder.column(columnName));
                    whereArgs.add(String.valueOf(idColumnValue));
                } catch (IllegalAccessException e) {
                    //e.printStackTrace();
                    Log.w(Cube.TAG, "Could not access field: " + field.getName(), e);
                }
            } else {
                if (!field.isAnnotationPresent(Id.class)) {
                    ReflectionUtil
                            .addFieldValueToColumn(getConfig(), values, field, object, entitiesMap);
                }
            }
        }

        if (idField != null) {
            String[] whereArgsArray = whereArgs.toArray(new String[whereArgs.size()]);
            // Get SugarRecord based on Unique values
            long rowsEffected = db.update(NameUtil.toTableName(object
                                                                       .getClass()), values, whereClause
                                                  .toString(), ParamUtil.fixArgs(whereArgsArray));

            //if (rowsEffected == 0) {
            //    // FIXME This is WRONG. The save method returns an ID, but this update method should
            //    // FIXME return a count. I am not sure of the implications of changing the count to 1
            //    // FIXME at this moment, so I'll have to come back to it. -bpappin
            //    final long recordId = save(db, object);
            //    return recordId;
            //} else {
            if (rowsEffected > 0) {

                try {
                    idField.setAccessible(true);
                    notifyChange(object.getClass(), Long
                            .valueOf(String.valueOf(idField.get(object))));
                } catch (IllegalAccessException e) {
                    Log.w(Cube.TAG, "Could not access field: " + idField.getName(), e);
                    notifyChange(object.getClass());
                }
            }
            return rowsEffected;
            //}
        }
        Log.w(Cube.TAG, "Cound not update, the entity did not have an @Id field.");
        return 0;
    }

    public <T> void notifyChange(Class<T> type) {
        notifyChange(Cube.createUri(type, null), null, false);
    }

    /**
     * Shortcut to ContentResolver.notifyChange(Uri uri, ContentObserver observer, boolean
     * syncToNetwork)
     *
     * @param uri
     * @param observer
     * @param syncToNetwork
     */
    public void notifyChange(Uri uri, ContentObserver observer, boolean syncToNetwork) {
        if (getContext() != null) {
            if (getConfig().isDebug()) {
                Log.d(Cube.TAG, "Notify of data change: " + uri);
            }
            getContext()
                    .getApplicationContext()
                    .getContentResolver()
                    .notifyChange(uri, observer, syncToNetwork);
        } else {
            Log.w(Cube.TAG, "Context not set. Unable to notify of data change: " + uri);
        }
    }

    /**
     * Convenience method for deleting all entities in the database.
     *
     * @param type The type of the entity.
     * @param <T>
     * @return int the number of rows
     */
    public <T> int deleteAll(Class<T> type) {
        // XXX Must set "1" as the wereClause so that the numner of rows deleted is returned.
        // XXX See SQLiteDatabase.delete(String table, String whereClause, String[] whereArgs)
        return deleteAll(type, "1");
    }

    /**
     * Convenience method for deleting entities in the database.
     *
     * @param type        The type of the entity.
     * @param whereClause the optional WHERE clause to apply when deleting. Passing null will delete
     *                    all rows.
     * @param whereArgs   ou may include ?s in the where clause, which will be replaced by the
     *                    values from whereArgs. The values will be bound as Strings.
     * @param <T>
     * @return int the number of rows affected if a whereClause is passed in, 0 otherwise. To remove
     * all rows and get a count pass "1" as the whereClause.
     */
    public <T> int deleteAll(Class<T> type, String whereClause, String... whereArgs) {
        if (type.isAnnotationPresent(View.class)) {
            // XXX can't update a view.
            return 0;
        }
        // Does notifyChange get called in these direct calls?
        return getDatabase().delete(NameUtil.toTableName(type), whereClause, ParamUtil.fixArgs(whereArgs));
    }

    /**
     * Convenience method for deleting a specific entity in the database.
     *
     * @param object the entity to delete. Must have a field marked with an @Id annotation.
     * @param <T>
     * @return int the number of entities deleted.
     */
    public <T> int delete(T object) {
        Class<?> type = object.getClass();
        if (type.isAnnotationPresent(View.class)) {
            // XXX can't delete a view.
            return 0;
        }
        if (type.isAnnotationPresent(Table.class)) {
            try {
                // FIXME this looks for the id field, but the annotated ID may be a different name.
                Field field = type.getDeclaredField("id");
                field.setAccessible(true);
                if (field.isAnnotationPresent(Id.class)) {
                    Long id = (Long)field.get(object);
                    if (id != null && id > 0L) {

                        int deleted = getDatabase().delete(NameUtil.toTableName(type), QueryBuilder
                                .id(), new String[]{
                                id.toString()
                        });
                        if (deleted > 0) {
                            Log.d(Cube.TAG, type.getSimpleName() + " deleted : " + id);
                            notifyChange(type, id);

                        } else {
                            Log.w(Cube.TAG, type.getSimpleName() + " was not deleted : " + id);
                        }
                        return deleted;
                    } else {
                        Log.w(Cube.TAG,
                              "Cannot delete object: " + object
                                      .getClass()
                                      .getSimpleName() +
                                      " - object has not been saved");
                        return 0;
                    }
                } else {
                    Log.w(Cube.TAG, "Cannot delete object: " + object
                            .getClass()
                            .getSimpleName() +
                            " - not field anotated with @Id.");
                    return 0;
                }
            } catch (NoSuchFieldException e) {
                Log.w(Cube.TAG, "Cannot delete object: " + object
                        .getClass()
                        .getSimpleName() +
                        " - annotated object has no id");
                return 0;
            } catch (IllegalAccessException e) {
                Log.w(Cube.TAG, "Cannot delete object: " + object
                        .getClass()
                        .getSimpleName() +
                        " - can't access id");
                return 0;
            }
            //} else if (SugarRecord.class.isAssignableFrom(type)) {
            //    return ((SugarRecord) object).delete();
        } else {
            Log.w(Cube.TAG, "Cannot delete object: " + object
                    .getClass()
                    .getSimpleName() +
                    " - not an entity");
            return 0;
        }
    }

    public <T> void notifyChange(Class<T> type, Long id) {
        notifyChange(Cube.createUri(type, id), null, false);
    }


    public <T> long count(Class<?> type) {
        return count(type, null, null, null, null, 0);
    }

    public <T> long count(Class<?> type, String whereClause, String[] whereArgs, String groupBy, String orderBy, int limit) {
        long result = -1;
        String filter = (!TextUtils.isEmpty(whereClause)) ? " where " + whereClause : "";
        SQLiteStatement sqliteStatement;
        try {
            sqliteStatement = getDatabase().compileStatement(
                    "SELECT count(*) FROM " +
                            NameUtil.toTableName(type) + filter);
        } catch (SQLiteException e) {
            //e.printStackTrace();
            Log.w(Cube.TAG, "Could not get entity count.", e);
            return result;
        }

        if (whereArgs != null) {
            for (int i = whereArgs.length; i != 0; i--) {
                sqliteStatement.bindString(i, whereArgs[i - 1]);
            }
        }

        try {
            result = sqliteStatement.simpleQueryForLong();
        } finally {
            sqliteStatement.close();
        }

        return result;
    }

    public <T> long count(Class<?> type, String whereClause, String[] whereArgs) {
        return count(type, whereClause, ParamUtil.fixArgs(whereArgs), null, null, 0);
    }

    public <T> void notifyChange(Class<T> type, ContentObserver observer, boolean syncToNetwork) {
        notifyChange(Cube.createUri(type, null), observer, syncToNetwork);
    }

    public <T> void notifyChange(Class<T> type, Long id, ContentObserver observer, boolean syncToNetwork) {
        notifyChange(Cube.createUri(type, id), observer, syncToNetwork);
    }

    public <T> List<T> findWithQuery(Class<T> type, String query, String... arguments) {
        Cursor cursor = getDatabase().rawQuery(query, ParamUtil.fixArgs(arguments));

        return getEntitiesFromCursor(cursor, type);
    }

    public <T> Iterator<T> findAll(Class<T> type) {
        return findAsIterator(type, null, null);
    }

    public <T> Iterator<T> findAsIterator(Class<T> type, String whereClause, String... whereArgs) {
        return findAsIterator(type, whereClause, ParamUtil.fixArgs(whereArgs), null, null, 0);
    }

    public <T> Iterator<T> findWithQueryAsIterator(Class<T> type, String query, String... arguments) {
        Cursor cursor = getDatabase().rawQuery(query, ParamUtil.fixArgs(arguments));
        return new CursorIterator<>(type, cursor);
    }

    public <T> Iterator<T> findAsIterator(Class<T> type, String whereClause, String[] whereArgs, String groupBy, String orderBy, int limit) {
        Cursor cursor = getDatabase()
                .query(NameUtil.toTableName(type), null, whereClause, ParamUtil.fixArgs(whereArgs),
                       groupBy, null, orderBy, Integer.toString(limit));
        return new CursorIterator<>(type, cursor);
    }

    class CursorIterator<E> implements Iterator<E> {
        Class<E> type;
        Cursor cursor;

        public CursorIterator(Class<E> type, Cursor cursor) {
            this.type = type;
            this.cursor = cursor;
        }

        @Override
        public boolean hasNext() {
            return cursor != null && !cursor.isClosed() && !cursor.isAfterLast();
        }

        @Override
        public E next() {
            E entity = null;
            if (cursor == null || cursor.isAfterLast()) {
                throw new NoSuchElementException();
            }

            if (cursor.isBeforeFirst()) {
                cursor.moveToFirst();
            }

            try {
                entity = type
                        .getDeclaredConstructor()
                        .newInstance();
                inflate(cursor, entity, getEntitiesMap());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.moveToNext();
                if (cursor.isAfterLast()) {
                    cursor.close();
                }
            }

            return entity;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}

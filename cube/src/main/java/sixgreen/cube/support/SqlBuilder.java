package sixgreen.cube.support;

import android.util.Log;

import java.lang.reflect.Field;
import java.util.List;

import sixgreen.cube.Cube;
import sixgreen.cube.CubeConfig;
import sixgreen.cube.annotation.Column;
import sixgreen.cube.annotation.MultiUnique;
import sixgreen.cube.annotation.NotNull;
import sixgreen.cube.annotation.Unique;
import sixgreen.cube.annotation.View;
import sixgreen.cube.util.KeyWordUtil;
import sixgreen.cube.util.NameUtil;
import sixgreen.cube.util.ReflectionUtil;

//import com.orm.helper.ManifestHelper;
//import com.orm.helper.NamingHelper;
//import com.orm.util.KeyWordUtil;
//import com.orm.util.QueryBuilder;
//import com.orm.util.ReflectionUtil;

/**
 * Created by bpappin on 16-08-23.
 */
public class SqlBuilder {
    public static final String NULL = " NULL";
    public static final String NOT_NULL = " NOT NULL";
    public static final String UNIQUE = " UNIQUE";

    public static String createViewSQL(CubeConfig config, Class<?> entity) {
        if (!entity.isAnnotationPresent(View.class)) {
            throw new RuntimeException(
                    "Entity " + entity.getSimpleName() + " is not annotated with View.");
        }

        if (config.isDebug()) {
            Log.i(Cube.TAG, "Create view if not exists");
        }

        String viewName = NameUtil.toTableName(entity);
        if (KeyWordUtil.isKeyword(viewName)) {
            Log.e(Cube.TAG, "SQLITE RESERVED WORD USED IN VIEW " + viewName);
        }
        StringBuilder sb = new StringBuilder("CREATE VIEW IF NOT EXISTS ");
        sb.append(viewName);
        sb.append(" AS ");


        View annotation = entity.getAnnotation(View.class);
        sb.append(annotation.select());

        if (config.isDebug()) {
            Log.i(Cube.TAG, "Create view statement: " + sb.toString());
        }

        return sb.toString();
    }

    public static String createTableSQL(CubeConfig config, Class<?> entity) {
        if (config.isDebug()) {
            Log.i(Cube.TAG, "Create entity if not exists");
        }
        List<Field> fields = ReflectionUtil.getTableFields(config, entity);
        String tableName = NameUtil.toTableName(entity);

        if (KeyWordUtil.isKeyword(tableName)) {
            Log.e(Cube.TAG, "SQLITE RESERVED WORD USED IN TABLE " + tableName);
        }

        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(tableName).append(" ( " + Cube._ID + " INTEGER PRIMARY KEY AUTOINCREMENT ");

        for (Field column : fields) {
            String columnName = NameUtil.toColumnName(column);
            String columnType = QueryBuilder.getColumnType(column.getType());

            if (columnType != null) {
                if (columnName.equalsIgnoreCase(Cube._ID)) {
                    if (config.isDebug()) {
                        Log.i(Cube.TAG, "Skipping _id column: already added.");
                    }
                    continue;
                }

                if (column.isAnnotationPresent(Column.class)) {
                    Column columnAnnotation = column.getAnnotation(Column.class);
                    columnName = columnAnnotation.name();

                    sb.append(", ").append(columnName).append(" ").append(columnType);

                    if (config.isDebug()) {
                        Log.i(Cube.TAG, "Adding column: " + columnName);
                        Log.i(Cube.TAG, "\t" + columnType);
                    }

                    if (columnAnnotation.notNull()) {
                        if (columnType.endsWith(NULL)) {
                            sb.delete(sb.length() - 5, sb.length());
                        }
                        sb.append(NOT_NULL);
                        if (config.isDebug()) {
                            Log.i(Cube.TAG, "\t" + NOT_NULL);
                        }
                    }

                    if (columnAnnotation.unique()) {
                        sb.append(UNIQUE);
                        if (config.isDebug()) {
                            Log.i(Cube.TAG, "\t" + UNIQUE);
                        }
                    }

                } else {
                    sb.append(", ").append(columnName).append(" ").append(columnType);
                    if (config.isDebug()) {
                        Log.i(Cube.TAG, "Adding column: " + columnName);
                        Log.i(Cube.TAG, "\t" + columnType);
                    }
                    if (column.isAnnotationPresent(NotNull.class)) {
                        if (columnType.endsWith(NULL)) {
                            sb.delete(sb.length() - 5, sb.length());
                        }
                        sb.append(NOT_NULL);
                        if (config.isDebug()) {
                            Log.i(Cube.TAG, "\t" + NOT_NULL);
                        }
                    }

                    if (column.isAnnotationPresent(Unique.class)) {
                        sb.append(UNIQUE);
                        if (config.isDebug()) {
                            Log.i(Cube.TAG, "\t" + UNIQUE);
                        }
                    }
                }
            }
        }

        if (entity.isAnnotationPresent(MultiUnique.class)) {
            String constraint = entity.getAnnotation(MultiUnique.class).value();
            //if (config.isDebug()) {
            //    Log.i(Cube.TAG, "Adding column: " + columnName);
            //    Log.i(Cube.TAG, "\t" + columnType);
            //}
            sb.append(", UNIQUE(");

            String[] constraintFields = constraint.split(",");
            for (int i = 0; i < constraintFields.length; i++) {
                String columnName = NameUtil.toSQLNameDefault(constraintFields[i]);
                sb.append(columnName);

                if (i < (constraintFields.length - 1)) {
                    sb.append(",");
                }
            }

            sb.append(") ON CONFLICT REPLACE");
        }

        sb.append(" ) ");
        if (config.isDebug()) {
            Log.i(Cube.TAG, "Create entity statement: " + sb.toString());
        }

        return sb.toString();
    }
}

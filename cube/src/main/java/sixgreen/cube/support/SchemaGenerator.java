package sixgreen.cube.support;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import sixgreen.cube.Cube;
import sixgreen.cube.CubeConfig;
import sixgreen.cube.annotation.Table;
import sixgreen.cube.annotation.View;
import sixgreen.cube.util.NameUtil;

/**
 * Based on the SchemaGenerator from Sugar ORM.
 * <p/>
 * Created by bpappin on 16-08-23.
 */
public class SchemaGenerator {

    private CubeConfig config;

    public SchemaGenerator(CubeConfig config) {
        super();
        this.config = config;
    }

    public static SchemaGenerator get(CubeConfig config) {
        final SchemaGenerator schemaGenerator = new SchemaGenerator(config);
        return schemaGenerator;
    }

    public void doCreate(SQLiteDatabase db) {
        if (config.isDebug()) {
            Log.i(Cube.TAG, "Creating database tables from entities...");
        }

        // XXX We need to loop twice to make sure we handle the views *after* the tables, since we
        // XXX don't want to have to mess with user config for make sure view come after tables.
        Class<?>[] entityClasses = config.getEntityClasses();
        for (Class<?> entity : entityClasses) {
            if (entity.isAnnotationPresent(Table.class)) {
                createTable(entity, db);
            }
        }
        for (Class<?> entity : entityClasses) {
            if (entity.isAnnotationPresent(View.class)) {
                createView(entity, db);
            }
        }
    }
    

    public void doUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (config.isDebug()) {
            Log.i(Cube.TAG, "Upgrading database from " + oldVersion + " to " + newVersion + "...");
        }
        dropAllTables(db);
        doCreate(db);
    }


    public void doDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (config.isDebug()) {
            Log.i(Cube.TAG,
                    "Downgrading database from " + oldVersion + " to " + newVersion + "...");
        }
        dropAllTables(db);
        doCreate(db);
    }

    public void doManualReset(SQLiteDatabase db) {
        if (config.isDebug()) {
            Log.i(Cube.TAG, "Resetting database manually...");
        }
        dropAllTables(db);
        doCreate(db);
    }

    public void dropAllTables(SQLiteDatabase db) {
        if (config.isDebug()) {
            Log.i(Cube.TAG, "Dropping all database tables...");
        }
        Class<?>[] entityClasses = config.getEntityClasses();
        for (Class<?> entity : entityClasses) {
            String sql = null;
            if (entity.isAnnotationPresent(View.class)) {
                sql = "DROP VIEW IF EXISTS " + NameUtil.toTableName(entity);
            } else {
                sql = "DROP TABLE IF EXISTS " + NameUtil.toTableName(entity);
            }
            if (sql != null) {
                //final String sql = "DROP TABLE IF EXISTS " + NameUtil.toTableName(entity);
                if (config.isLogQueries()) {
                    Log.d(Cube.TAG, sql);
                }
                db.execSQL(sql);
            }
        }
    }

    private void createView(Class<?> entity, SQLiteDatabase db) {
        String createSQL = SqlBuilder.createViewSQL(config, entity);

        if (!createSQL.isEmpty()) {
            try {
                if (config.isLogQueries()) {
                    Log.d(Cube.TAG, createSQL);
                }
                db.execSQL(createSQL);
            } catch (SQLException e) {
                Log.e(Cube.TAG, "Unable to create table from entity.", e);
                //e.printStackTrace();
            }
        }
    }

    private void createTable(Class<?> entity, SQLiteDatabase db) {
        String createSQL = SqlBuilder.createTableSQL(config, entity);

        if (!createSQL.isEmpty()) {
            try {
                if (config.isLogQueries()) {
                    Log.d(Cube.TAG, createSQL);
                }
                db.execSQL(createSQL);
            } catch (SQLException e) {
                Log.e(Cube.TAG, "Unable to create table from entity.", e);
                //e.printStackTrace();
            }
        }
    }
    

}

package sixgreen.cube;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sixgreen.cube.support.CubeCursorFactory;
import sixgreen.cube.support.CubeDataManager;
import sixgreen.cube.support.CubeErrorHandler;
import sixgreen.cube.support.CubeManifest;

/**
 * Created by bpappin on 16-08-11.
 */
public abstract class CubeConfig {
    private boolean debug;
    private boolean logQueries;
    private boolean writeAheadLoggingEnabled = true;
    private boolean transactionsEnabled = true;
    private DatabaseErrorHandler databaseErrorHandler = new CubeErrorHandler();
    private CursorFactory cursorFactory;

    private final static Map<Class<?>, List<Field>> fieldCache = new HashMap<>();

    public static final CubeConfig manifest(Context context) {
        return CubeManifest.getConfig(context);
    }

    public boolean isTransactionsEnabled() {
        return transactionsEnabled;
    }

    public void setTransactionsEnabled(boolean transactionsEnabled) {
        this.transactionsEnabled = transactionsEnabled;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public boolean isLogQueries() {
        return logQueries;
    }

    public void setLogQueries(boolean logQueries) {
        this.logQueries = logQueries;
    }

    /**
     * @return
     * @see {https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html#enableWriteAheadLogging()}
     */
    public boolean isWriteAheadLoggingEnabled() {
        return writeAheadLoggingEnabled;
    }

    /**
     * @param writeAheadLoggingEnabled
     * @see {https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html#enableWriteAheadLogging()}
     */
    public void setWriteAheadLoggingEnabled(boolean writeAheadLoggingEnabled) {
        this.writeAheadLoggingEnabled = writeAheadLoggingEnabled;
    }

    /**
     * the {@link DatabaseErrorHandler} to be used when sqlite reports database
     */
    public DatabaseErrorHandler getDatabaseErrorHandler() {
        return databaseErrorHandler;
    }

    /**
     * the {@link DatabaseErrorHandler} to be used when sqlite reports database
     */
    public void setDatabaseErrorHandler(DatabaseErrorHandler databaseErrorHandler) {
        this.databaseErrorHandler = databaseErrorHandler;
    }

    /**
     * The class to use for creating cursor objects, or null for the default
     *
     * @return CursorFactory
     */
    public CursorFactory getCursorFactory() {
        if (this.cursorFactory != null) {
            return this.cursorFactory;
        }
        return new CubeCursorFactory(isLogQueries());
    }

    /**
     * The class to use for creating cursor objects, or null for the default
     *
     * @param cursorFactory
     *         CursorFactory
     */
    public void setCursorFactory(CursorFactory cursorFactory) {
        this.cursorFactory = cursorFactory;
    }

    public static List<Field> getFieldCache(Class<?> clazz) {
        if (Cube.getConfig().isDebug()) {
            Log.d(Cube.TAG, "Get field cache for: " + clazz.getSimpleName());
        }
        if (fieldCache.containsKey(clazz)) {
            List<Field> list = fieldCache.get(clazz);
            if (Cube.getConfig().isDebug()) {
                Log.d(Cube.TAG, "\tFound " + list.size() + " cached fields.");
            }
            return Collections.synchronizedList(list);
        }
        if (Cube.getConfig().isDebug()) {
            Log.d(Cube.TAG, "\tFound no cached fields.");
        }
        return null;
    }

    public void setFieldCache(Class<?> table, List<Field> toStore) {
        fieldCache.put(table, toStore);
    }

    /**
     * The version number of the database (starting at 1); if the database is older,
     * {@link CubeDataManager#onUpgrade} will be used to upgrade the database; if the database is
     * newer, {@link CubeDataManager#onDowngrade} will be used to downgrade the database
     *
     * @return int the version of this database
     */
    public abstract int getVersion();

    /**
     * The name of the database file, or null for an in-memory database.
     *
     * @return String the name of this database
     */
    public abstract String getDatabaseName();

    /**
     * A collections of classes that represent entities for Cube.
     *
     * @return Class<?>[] an array of the entity classes for the schema.
     */
    public abstract Class<?>[] getEntityClasses();

    /**
     * The content provider authority. This will match the authority string in the manifest
     * for the ContentProvider entry.
     *
     * @return String
     */
    public abstract String getAuthority();

}

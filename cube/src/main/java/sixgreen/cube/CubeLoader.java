package sixgreen.cube;

import sixgreen.cube.support.CubeDataManager;

/**
 * Convenience base class for objects that need to be able to load themselves.
 * There is no requirement for persistent objects to extend this
 * class; it just provides a load() method.
 * <p>
 * There are two versions of the load() method. The no-args version does the
 * load operation inside its own transaction - it's the simplest way of loading
 * an object. The other version takes a CubeDataManager object and adds the load operation to the
 * mangers transaction.
 * <p>
 * <p>
 * Created by bpappin on 16-08-23.
 */
public abstract class CubeLoader<T>  extends CubeBase{
    /**
     * @param manager
     *         the CubeDataManager to work with.
     * @return T
     */
    public abstract T load(CubeDataManager manager);

    /**
     * @return T
     * @throws Exception
     */
    public T load() throws RuntimeException {
        return Cube.executeLoader(this);
    }
}

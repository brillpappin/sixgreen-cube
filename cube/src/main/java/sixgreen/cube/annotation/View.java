package sixgreen.cube.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Creates a SQLite read only view that can be queried.
 * <p/>
 * The @Id annotation is ignored for views, as they are read only.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface View {
    /**
     * The name of the view.
     *
     * @return
     */
    String name() default "";

    /**
     * The select statement for the view.
     *
     * @return
     */
    String select() default "";
}

package sixgreen.cube;

import sixgreen.cube.util.NameUtil;

/**
 * Created by bpappin on 2016-08-29.
 */
public class CubeBase {
    /**
     * returns a string in for form "name = ?".
     *
     * @param name
     *         the name of the column to add.
     * @return String "name = ?"
     */
    public String eq(String name) {
        return name + " = ?";
    }

    /**
     * @param name
     *         the name of the column.
     * @return String "name != ?"
     */
    public String notEq(String name) {
        return name + " != ?";
    }

    /**
     * @param field
     * @param last
     * @return String "field[,]"
     */
    public String order(String field, boolean last) {
        return field + (last ? "" : ", ");
    }

    /**
     * @param field
     * @param last
     * @return String "field DESC[,]"
     */
    public String desc(String field, boolean last) {
        return field + " DESC" + (last ? "" : ", ");
    }

    /**
     * @param field
     * @param last
     * @return String "field ASC[,]"
     */
    public String asc(String field, boolean last) {
        return field + " ASC" + (last ? "" : ", ");
    }

    /**
     * @param entity
     * @param <T>
     * @return the table name linked the the specified entity.
     */
    public <T> String table(Class<T> entity) {
        return NameUtil.toTableName(entity);
    }
}

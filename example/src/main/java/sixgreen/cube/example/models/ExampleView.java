package sixgreen.cube.example.models;

import sixgreen.cube.annotation.Column;
import sixgreen.cube.annotation.View;

/**
 * Created by bpappin on 2016-09-18.
 */
@View(name = "example_view", select = "select xmpl_first, xmpl_second from example_one, example_two ORDER BY xmpl_first")
public class ExampleView {

    @Column(name = "xmpl_first")
    private String first;

    @Column(name = "xmpl_second")
    private String second;

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }
}

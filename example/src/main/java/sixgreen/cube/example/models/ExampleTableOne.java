package sixgreen.cube.example.models;

import sixgreen.cube.annotation.Column;
import sixgreen.cube.annotation.Id;
import sixgreen.cube.annotation.Table;

/**
 * Created by bpappin on 2016-09-18.
 */
@Table(name = "example_one")
public class ExampleTableOne {

    @Id
    private long id;

    @Column(name = "xmpl_first")
    private String first;

    @Column(name = "xmpl_created")
    private long created;

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }
}

package sixgreen.cube.example.models;

import sixgreen.cube.annotation.Column;
import sixgreen.cube.annotation.Id;
import sixgreen.cube.annotation.Table;

/**
 * Created by bpappin on 2016-09-18.
 */
@Table(name = "example_two")
public class ExampleTableTwo {

    @Id
    private long id;

    @Column(name = "xmpl_second")
    private String second;

    @Column(name = "xmpl_created")
    private long created;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }
}

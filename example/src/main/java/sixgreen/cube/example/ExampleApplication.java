package sixgreen.cube.example;

import android.app.Application;

import sixgreen.cube.Cube;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Cube.setup(getApplicationContext());
    }
}

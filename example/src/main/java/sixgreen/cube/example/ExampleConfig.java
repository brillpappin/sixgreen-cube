package sixgreen.cube.example;

import sixgreen.cube.CubeConfig;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.example.models.ExampleTableTwo;
import sixgreen.cube.example.models.ExampleView;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleConfig extends CubeConfig {

    private static final int VERSION = 4;
    private static final boolean DEBUG = true;
    private static final boolean LOG_QUERIES = true;
    public static final String DATABASE = "example_cube_database.db";
    public static final String AUTHORITY = "com.sixgreen.cube.example";
    public static final Class<?>[] ENTITIES = {
            ExampleTableOne.class,
            ExampleTableTwo.class,
            ExampleView.class
    };

    public ExampleConfig() {
        super();
        setDebug(DEBUG);
        setLogQueries(LOG_QUERIES);
    }

    /**
     * The version number of the database (starting at 1); if the database is older,
     * {@link CubeDataManager#onUpgrade} will be used to upgrade the database; if the database is
     * newer, {@link CubeDataManager#onDowngrade} will be used to downgrade the database
     *
     * @return int the version of this database
     */
    @Override
    public int getVersion() {
        return VERSION;
    }

    /**
     * The name of the database file, or null for an in-memory database.
     *
     * @return String the name of this database
     */
    @Override
    public String getDatabaseName() {
        return DATABASE;
    }

    /**
     * A collections of classes that represent entities for Cube.
     *
     * @return Class<?>[] an array of the entity classes for the schema.
     */
    @Override
    public Class<?>[] getEntityClasses() {
        return ENTITIES;
    }

    /**
     * The content provider authority. This will match the authority string in the manifest
     * for the ContentProvider entry.
     *
     * @return String
     */
    @Override
    public String getAuthority() {
        return AUTHORITY;
    }
}

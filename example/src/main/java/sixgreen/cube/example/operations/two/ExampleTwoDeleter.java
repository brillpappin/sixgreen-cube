package sixgreen.cube.example.operations.two;

import sixgreen.cube.CubeDeleter;
import sixgreen.cube.example.models.ExampleTableTwo;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleTwoDeleter extends CubeDeleter {
    private ExampleTableTwo entity;

    public void setEntity(ExampleTableTwo entity) {
        this.entity = entity;
    }

    @Override
    public int delete(CubeDataManager manager) {
        return manager.delete(entity);
    }
}

package sixgreen.cube.example.operations.one;

import java.util.List;

import sixgreen.cube.CubeLoader;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleOneListAllLoader extends CubeLoader<List<ExampleTableOne>> {

    /**
     * @param manager
     *         the CubeDataManager to work with.
     * @return T
     */
    @Override
    public List<ExampleTableOne> load(CubeDataManager manager) {
        return manager.find(ExampleTableOne.class, null, null);
    }
}

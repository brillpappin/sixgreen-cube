package sixgreen.cube.example.operations.one;

import sixgreen.cube.CubeUpdater;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleOneUpdater extends CubeUpdater {
    private ExampleTableOne entity;

    public void setEntity(ExampleTableOne entity) {
        this.entity = entity;
    }

    /**
     * Save the object in an existing session.
     *
     * @param manager
     *         JPA EntityManager manager.
     */
    @Override
    public void update(CubeDataManager manager) {
        manager.update(entity);
    }
}

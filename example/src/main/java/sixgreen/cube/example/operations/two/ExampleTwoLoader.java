package sixgreen.cube.example.operations.two;

import sixgreen.cube.CubeLoader;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.example.models.ExampleTableTwo;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleTwoLoader extends CubeLoader<ExampleTableTwo> {
    private long id;

    public void setId(long id) {
        this.id = id;
    }

    /**
     * @param manager
     *         the CubeDataManager to work with.
     * @return T
     */
    @Override
    public ExampleTableTwo load(CubeDataManager manager) {
        return manager.load(ExampleTableTwo.class, id);
    }
}

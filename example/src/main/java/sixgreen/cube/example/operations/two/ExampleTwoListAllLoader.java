package sixgreen.cube.example.operations.two;

import java.util.List;

import sixgreen.cube.CubeLoader;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.example.models.ExampleTableTwo;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleTwoListAllLoader extends CubeLoader<List<ExampleTableTwo>> {

    /**
     * @param manager
     *         the CubeDataManager to work with.
     * @return T
     */
    @Override
    public List<ExampleTableTwo> load(CubeDataManager manager) {
        return manager.find(ExampleTableTwo.class, null, null);
    }
}

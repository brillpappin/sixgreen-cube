package sixgreen.cube.example.operations.two;

import sixgreen.cube.CubeSaver;
import sixgreen.cube.example.models.ExampleTableTwo;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleTwoSaver extends CubeSaver {
    private ExampleTableTwo entity;

    public void setEntity(ExampleTableTwo entity) {
        this.entity = entity;
    }

    @Override
    public void save(CubeDataManager manager) {
        entity.setCreated(System.currentTimeMillis());
        manager.save(entity);
    }
}

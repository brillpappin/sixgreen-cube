package sixgreen.cube.example.operations.one;

import sixgreen.cube.CubeSaver;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleOneSaver extends CubeSaver {
    private ExampleTableOne entity;

    public void setEntity(ExampleTableOne entity) {
        this.entity = entity;
    }

    @Override
    public void save(CubeDataManager manager) {
        entity.setCreated(System.currentTimeMillis());
        manager.save(entity);
    }
}

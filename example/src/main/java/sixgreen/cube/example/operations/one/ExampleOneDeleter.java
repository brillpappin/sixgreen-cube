package sixgreen.cube.example.operations.one;

import sixgreen.cube.CubeDeleter;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleOneDeleter extends CubeDeleter {
    private ExampleTableOne entity;

    public void setEntity(ExampleTableOne entity) {
        this.entity = entity;
    }

    @Override
    public int delete(CubeDataManager manager) {
        return manager.delete(entity);
    }
}

package sixgreen.cube.example.operations.one;

import sixgreen.cube.CubeDeleter;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleOneResetDeleter extends CubeDeleter {

    @Override
    public int delete(CubeDataManager manager) {
        return manager.deleteAll(ExampleTableOne.class);
    }
}

package sixgreen.cube.example.operations.view;

import java.util.List;

import sixgreen.cube.CubeLoader;
import sixgreen.cube.example.models.ExampleView;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleViewLoader extends CubeLoader<List<ExampleView>> {
    /**
     * @param manager
     *         the CubeDataManager to work with.
     * @return T
     */
    @Override
    public List<ExampleView> load(CubeDataManager manager) {

        List<ExampleView> views = manager.view(ExampleView.class);
        return views;
    }
}

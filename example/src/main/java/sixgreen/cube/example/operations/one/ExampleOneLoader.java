package sixgreen.cube.example.operations.one;

import sixgreen.cube.CubeLoader;
import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.support.CubeDataManager;

/**
 * Created by bpappin on 2016-09-18.
 */
public class ExampleOneLoader extends CubeLoader<ExampleTableOne> {
    private long id;

    public void setId(long id) {
        this.id = id;
    }

    /**
     * @param manager
     *         the CubeDataManager to work with.
     * @return T
     */
    @Override
    public ExampleTableOne load(CubeDataManager manager) {
        return manager.load(ExampleTableOne.class, id);
    }
}

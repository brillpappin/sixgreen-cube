package sixgreen.cube.example;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;
import java.util.UUID;

import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.example.models.ExampleTableTwo;
import sixgreen.cube.example.models.ExampleView;
import sixgreen.cube.example.operations.one.ExampleOneSaver;
import sixgreen.cube.example.operations.two.ExampleTwoSaver;
import sixgreen.cube.example.operations.view.ExampleViewLoader;

public class MainActivity extends AppCompatActivity {
    
    private static final String TAG = "MainActivity";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExampleTableOne one = new ExampleTableOne();
                one.setFirst("time:" + System.currentTimeMillis());
                ExampleOneSaver oneSaver = new ExampleOneSaver();
                oneSaver.setEntity(one);
                oneSaver.save();

                ExampleTableTwo two = new ExampleTableTwo();
                two.setSecond(UUID.randomUUID().toString());
                ExampleTwoSaver twoSaver = new ExampleTwoSaver();
                twoSaver.setEntity(two);
                twoSaver.save();

                Snackbar.make(view, "Records created...", Snackbar.LENGTH_LONG)
                        .show();
            }
        });

        FloatingActionButton fabView = (FloatingActionButton) findViewById(R.id.fabView);
        fabView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "Loading ExampleView...");

                ExampleViewLoader loader = new ExampleViewLoader();
                List<ExampleView> exmpl = loader
                        .load();

                for (ExampleView n : exmpl) {
                    Log.d(TAG, "View: " + n.getFirst() + ", " + n.getSecond());
                }
                //while (exmpl.hasNext()) {
                //    ExampleView n = exmpl.next();
                //    Log.d(TAG, "View: " + n.getFirst() + ", " + n.getSecond());
                //}

                Snackbar.make(view, "Records viewed...", Snackbar.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

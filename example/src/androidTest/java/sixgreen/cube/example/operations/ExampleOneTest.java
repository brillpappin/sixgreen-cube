package sixgreen.cube.example.operations;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import sixgreen.cube.example.models.ExampleTableOne;
import sixgreen.cube.example.operations.one.ExampleOneDeleter;
import sixgreen.cube.example.operations.one.ExampleOneListAllLoader;
import sixgreen.cube.example.operations.one.ExampleOneLoader;
import sixgreen.cube.example.operations.one.ExampleOneResetDeleter;
import sixgreen.cube.example.operations.one.ExampleOneSaver;
import sixgreen.cube.example.operations.one.ExampleOneUpdater;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by bpappin on 2016-09-19.
 */
public class ExampleOneTest {

    private String testData1;
    private String testData2;

    private ExampleTableOne data;

    private ExampleOneSaver saver = new ExampleOneSaver();
    private ExampleOneUpdater updater = new ExampleOneUpdater();
    private ExampleOneLoader loader = new ExampleOneLoader();
    private ExampleOneListAllLoader allLoader = new ExampleOneListAllLoader();
    ExampleOneDeleter deleter = new ExampleOneDeleter();
    ExampleOneResetDeleter resetter = new ExampleOneResetDeleter();


    @Before
    public void setUp() throws Exception {
        testData1 = "time:" + System.currentTimeMillis();

        data = new ExampleTableOne();
        data.setFirst(testData1);
    }

    @After
    public void tearDown() {
        resetter.delete();
    }

    @Test
    public void testSaveAndLoadAndDelete() throws Exception {
        saver.setEntity(data);
        saver.save();

        assertTrue(data.getId() > 0);

        loader.setId(data.getId());
        ExampleTableOne actual = loader.load();
        assertNotNull(actual);
        assertEquals(testData1, actual.getFirst());

        deleter.setEntity(actual);
        int deleted = deleter.delete();
        assertEquals(1, deleted);

        assertDatabaseIsEmpty(ExampleTableOne.class);
    }
    
    private void assertDatabaseIsEmpty(Class<ExampleTableOne> classUT) {
        List<ExampleTableOne> actual = allLoader.load();
        assertNotNull("Expected an empty list.", actual);
        assertEquals("Expected the database to be empty.", 0, actual.size());
    }
    
    @Test
    public void testSaveAndUpdate() throws Exception {
        saver.setEntity(data);
        saver.save();

        assertTrue(data.getId() > 0);



        data.setFirst(testData2);

        updater.setEntity(data);
        updater.update();

        loader.setId(data.getId());
        ExampleTableOne actual = loader.load();
        assertNotNull(actual);
        assertEquals(testData2, actual.getFirst());
    }
}
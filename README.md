# Cube ORM #

> ## NO LONGER MAINTAINED
> Cube ORM has been supplanted by [Room](https://developer.android.com/topic/libraries/architecture/room), and is no longer maintained.
> 
> _Use at your own risk_

**Cube ORM** is a __direct decendent__ of [Sugar ORM](https://github.com/satyan/sugar), and builds heavily on the great work done by it's developers.
The name __"Cube ORM"__ comes from making something new from sugar, a la 'Sugar Cube'. You may find parts of this documentation paraphrased from the Sugar ORM folks, since a lot of it came from them.

Cube takes the "Insanely easy way to work with Android databases" features of Sugar ORM, and extends them to use discrete, unit test friendly, loaders and updaters, as well as providing a basic ContentProvider, so that Cube entities can use the standard [Android Loader pattern](https://developer.android.com/guide/components/loaders.html).

### General Features ###

* Most of the features of Sugar ORM.
* Simple integration. You only need to set up a few things to get Cube working.
* Automatic table and column naming though reflection or annotations.
* Use of the Android standard identity column "_id" as defined in [BaseColumns._id](https://developer.android.com/reference/android/provider/BaseColumns.html)
* A basic [ContentProvider](https://developer.android.com/reference/android/content/ContentProvider.html) for use with the Android [Loaders](https://developer.android.com/guide/components/loaders.html).
* Simple java bean architecture, no need extend a base class to make Cube work.
* Cross-compatible with network frameworks like [Retrofit](http://square.github.io/retrofit/). You can use Cube Entities as Retrofit models.

##### What's different from Sugar ORM #####

* The identity coloumn is **_id** instead of **id**.
* Entities never extend a base object.
* Loaders, Savers, Updaters, Deleters can be nested so they are all in the same transaciton.
* Configuration is primarily done through a class instance.
* Android standard ContentProvider implementation.
* Drop on upgrade. No migration between database versions!

## Installing

There are three primary ways to install Cube:

#### As a Dependency


If you are using Gradle, simply add the dependency:

```groovy
compile 'com.sixgreen.android:cube:1.0.16-SNAPSHOT@aar'
```

You can add the dependency in Maven in a similar way:

```xml
<dependency>
    <groupId>com.sixgreen.android</groupId>
    <artifactId>cube</artifactId>
    <version>1.0.17-SNAPSHOT</version>
	<type>aar</type>
</dependency>
```

#### As a local Maven Install

You can install any branch of the code into your local maven repository.

You can add the local repository in the project build or your module build, but add it you must. This example shows the local repository in the project build:

```groovy
allprojects {
	repositories {
		mavenLocal()
	}
}
```

Checkout the source code from Git, and run the shell script `./install.sh`.
You can then include the library in your build as a dependency, the same way you would have done above __(see: As a Dependency)__.

#### As a Module

Checkout the source code from Git, and import it as a module. The project is available in the folder **cube**. For more information on how to do this, read [here](http://developer.android.com/tools/projects/index.html#LibraryProjects).

## Using Cube

### Creating an Entity ###

Database entities in Cube are simple java beans, annotated with the `Entity` annotaion. Although Cube will attempt to auto-generate the entity and column names, it is **strongly** recomended that you explicity name the columns, so that refactoring during development doesn't suddenly break your application at runtime.

As a matter of course, I will usually create an interface with the column names, and use those constants to name my columns _similar to the use of BaseColumns.\_ID below_. 
Useing this pattern ensures that my columns don't change without explicit knowledge and purpose. That becomes even more important when using the raw column names in your Android Loaders to do standard SQlite queries. 

Using annotations to name your tables and columns can also help prevent proguard from renaming things unexpectedly.

```java
@Table(name = "my_widget")
public class MyWidget {
    @Id
    @Column(name = BaseColumns._ID)
    private Long id;

    @Column(name = "my_name")
    private String name;
	
	public MyWidget() {
	        super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
```

### Configuring Cube ###
Cube has three things you need to do, to configure it.

1. Create a configuration class by extending `sixgreen.cube.CubeConfig`.
2. Initialize Cube with `Cube.setup(context)` in the Application class.
3. Add the provider and the metadata to the Manifest.

All three steps must be completed for Cube to work properly.

##### Create a configuration class

```java

public class MyCubeConfig extends CubeConfig {
    private static final int VERSION = 18;
    private static final boolean DEBUG = true;
    private static final boolean LOG_QUERIES = true;
    public static final String DATABASE = "my_cube_database.db";
    public static final String AUTHORITY = "my.package.provider.cube";
    public static final Class<?>[] entities = {
            MyWidget.class,
			NyOtherEntity.class
    };

    public MyCubeConfig() {
        super();
		setDebug(DEBUG);
		setLogQueries(LOG_QUERIES);
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public Class<?>[] getEntityClasses() {
        return entities;
    }

    @Override
    public String getDatabaseName() {
        return DATABASE;
    }
    
    @Override
    public String getAuthority() {
        return AUTHORITY;
    }
}
```

##### Initialize your application

The applicaiton context needs to be initialized in the Applicaiton object. This allows most Cube code to reside in a library or module and be initialized depending on the applicaiton that's running it.

```java
public class MyApplication extends Application {
    @Override
    public void onCreate() {
    	super.onCreate();
		
		Cube.setup(getApplicationContext());
	} 
}
```

##### Configure your manifest file

> Warning: the `authority` you used in your configuration class and the authorify you use to configure the provider **must be the same**.

```xml

<?xml version="1.0" encoding="utf-8"?>
<manifest package="conx2share.common"
          xmlns:android="http://schemas.android.com/apk/res/android">

    <application android:name="my.package.name.MyApplication"
        android:allowBackup="true"
        android:supportsRtl="true">

        <meta-data
            android:name="CUBE_CONFIG_CLASS"
            android:value="my.package.name.MyCubeConfig"/>

        <provider
            android:name="sixgreen.cube.provider.CubeContentProvider"
            android:authorities="my.package.provider.cube"
            android:exported="false"
            android:syncable="true"/>
			
			...
	</application>

```

## Entity CRUD: Load, Save, Update, Delete ###

Cube uses the concept of a Loader/Saver/Udpater/Delete as an operation on data.

You call one of these entity operations by creating it, and calling its action method.
```java
	MyWidgetSaver saver = new MyWidgetSaver();
	saver.setWidget(myWidget);
	saver.save();
```

This gives amazing flexability in terms of what happens to an entity whent he action is executed.
The actions can be nested inside each other and act upon the same transaction as well.


##### Loader implementation

Load a single entity by ID.

```java
public class MyWidgetLoader extends CubeLoader<MyWidget> {
    private long id;

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public MyWidget load(CubeDataManager manager) {
		MyWidget result = manager.load(MyWidget.class, id);
        return result;
    }
}
```

Or load a collection of entities based on a query.
You could use any method of creating a query, including Sugar ORM's Select class, or wtiging it by name.

```java
public class MyWidgetLoader extends CubeLoader<List<MyWidget>> {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<MyWidget> load(CubeDataManager manager) {
		// XXX Note that the condition is using the column real name, not the entity name.
	    List<MyWidget> result = Select.from(MyWidget.class)
	                                   .where(Condition.prop("my_name").eq(name))
	                                   .list(manager);
        return result;
    }
}
```

##### Saver implementation

Avoid doing an update in a CubeSaver.

```java
public class MyWidgetSaver extends CubeSaver {
    private MyWidget widget;

    public void setWidget(MyWidget widget) {
        this.widget = widget;
    }

    @Override
    public void save(CubeDataManager manager) {
		// I can do other operations on the widget object here before I save, 

        manager.save(widget);
    }
}
```

##### Updater and Deleter implementations

Updaters and Deleters use the same patterns. Simply extend CubeUpdater or CubeDeleter.

##### Transactions and Nesting CRUD operations

Operations can be nested, and run in the same transaction. This is good in a number of ways, but basically it means you only need to maintain a specific query once, and you can reuse it. It also helps significantly with efficiency.

This example uses a loader to get an entity, change it, then save it again.

```java
public class MyWidgetSaver extends CubeUpdater {
    private String oldName;
	private String newName;

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }

    @Override
    public void save(CubeDataManager manager) {
		MyWidgetByNameLoader loader = new MyWidgetByNameLoader();
		loader.setName(oldName);
		// XXX Note that we use the CubeDataManager we have already, to keep everything in the same transaction.
		MyWidget widget = loader.load(manager);
		
		widget.setName(newName);

        manager.save(widget);
    }
}
```


### Using Android Loaders ###

> Note: Loaders are differnt from a CubeLoader, they share the same function, but use differen arcitecture and patterns.

Read more about [Android Loaders](https://developer.android.com/guide/components/loaders.html) to get the full documentation, but the important part for this documentatioin is the onCreateLoader method, which supports the interaction between Cube and the ContentProvider.

```java
public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    // Generate the uri for the entity with the Cube class.
    Uri baseUri = Cube.createUri(MyWidget.class, null);
	
    String select = "((my_name NOTNULL) AND (my_name = ?)";
	String[] selectArgs = "John Doe";
	String[] orderBy = "my_name DESC";
			
	// XXX Note, the projection is null at this point. Cube returns all fields every time.
	// XXX Future implementations may allow a projections as well.
    return new CursorLoader(getActivity(), baseUri, null, select, selectArgs, orderBy);
}
```

## When using ProGuard
ProGuard can relly mess this stuff up, because it can mess with class and member names.
```java
 	# Ensures entities remain un-obfuscated so table and columns are named correctly
	-keep class com.yourpackage.yourapp.domainclasspackage.** { *; }
```

## Changes that are being considered

There are several areas that are being considered for improvement. 

* Requireing all entities tables to be named in their annotation.
* Requiring all columns to be annotated and named.
* Implement batch processing.
* Allow projections for the content provider.
* Mulltiple providers/databases.
* Allow custom migration paths between database versions.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


- - - -
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
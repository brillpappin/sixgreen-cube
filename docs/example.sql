DROP TABLE IF EXISTS example_one;
CREATE TABLE IF NOT EXISTS example_one ( _id INTEGER PRIMARY KEY AUTOINCREMENT , xmpl_created INTEGER, xmpl_first TEXT );
INSERT INTO `example_one`(`xmpl_created`,`xmpl_first`) VALUES (1, "first1");
INSERT INTO `example_one`(`xmpl_created`,`xmpl_first`) VALUES (1, "first2");
SELECT * FROM `example_one`

DROP TABLE IF EXISTS example_two;
CREATE TABLE IF NOT EXISTS example_two ( _id INTEGER PRIMARY KEY AUTOINCREMENT , xmpl_created INTEGER, xmpl_second TEXT );
INSERT INTO `example_two`(`xmpl_created`,`xmpl_second`) VALUES (1, "second1");
INSERT INTO `example_two`(`xmpl_created`,`xmpl_second`) VALUES (1, "second2");
SELECT * FROM `example_two`

DROP VIEW IF EXISTS example_view_0;
CREATE VIEW IF NOT EXISTS example_view_0 AS select xmpl_first, xmpl_second from example_one, example_two ORDER BY xmpl_first;
SELECT * FROM `example_view_0`

DROP VIEW IF EXISTS example_view_1;
CREATE VIEW IF NOT EXISTS example_view_1 AS select * from example_one, example_two;
SELECT * FROM `example_view_1`

DROP VIEW IF EXISTS example_view_2;
CREATE VIEW IF NOT EXISTS example_view_2 AS select o._id AS _id, * from example_one AS o, example_two AS t;
SELECT * FROM `example_view_2`;

DROP VIEW IF EXISTS example_view_3;
CREATE VIEW IF NOT EXISTS example_view_3 AS select o._id AS _id, * from example_one AS o, example_two AS t;
SELECT * FROM `example_view_3`;

DROP VIEW IF EXISTS example_view_4;
CREATE VIEW IF NOT EXISTS example_view_4 AS select o._id AS _id, group_concat(t.xmpl_second) AS second from example_one AS o, example_two AS t GROUP BY t.xmpl_second;
SELECT * FROM `example_view_4`;
# SELECT Column1, group_concat(Column2) FROM Table GROUP BY Column1


CREATE VIEW IF NOT EXISTS example_view_5 AS select o._id AS _id, group_concat(t.xmpl_second) AS second from example_one AS o, example_two AS t GROUP BY t.xmpl_second;
DROP VIEW IF EXISTS example_view_5;
SELECT * FROM `example_view_5`;
# SELECT Column1, group_concat(Column2) FROM Table GROUP BY Column1

